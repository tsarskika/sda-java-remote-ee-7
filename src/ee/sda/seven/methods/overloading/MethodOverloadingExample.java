package ee.sda.seven.methods.overloading;

public class MethodOverloadingExample {

    public static void main(String[] args) {

        // Method overloading with the example
        // When number of arguments are changed
        Adder adder = new Adder();

        adder.add(1, 4);
        adder.add(1, 4, 5);
        adder.add(1, 4, 5, 6);

        // When method argument(parameters) type is changed
        FormatService formatService = new FormatService();

        String years = formatService.formatNumber(6);
        String money = formatService.formatNumber(50.60);

        System.out.println(years);
        System.out.println(money);
    }

}
